import 'package:smap_studio_test_app/models/models.dart';

class ItemSearchState {
  final bool isLoading;
  final List<JsonItem> items;
  final bool hasError;
  final String oldQuery;

  const ItemSearchState(
      {this.isLoading, this.items, this.hasError, this.oldQuery});

  factory ItemSearchState.initial() {
    return ItemSearchState(
      items: [],
      isLoading: false,
      hasError: false,
    );
  }

  factory ItemSearchState.loading() {
    return ItemSearchState(
      items: [],
      isLoading: true,
      hasError: false,
    );
  }

  factory ItemSearchState.success(List<JsonItem> items, String oldQuery) {
    return ItemSearchState(
      oldQuery: oldQuery,
      items: items,
      isLoading: false,
      hasError: false,
    );
  }

  factory ItemSearchState.error() {
    return ItemSearchState(
      items: [],
      isLoading: false,
      hasError: true,
    );
  }

  @override
  String toString() =>
      'ItemSearchState {items: ${items.toString()}, isLoading: $isLoading, hasError: $hasError }';
}

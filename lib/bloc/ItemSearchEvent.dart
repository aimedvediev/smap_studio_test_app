class ItemSearchEvent {
  final String query;

  const ItemSearchEvent(this.query);

  @override
  String toString() => 'ItemSearchEvent{ query: $query}';
}
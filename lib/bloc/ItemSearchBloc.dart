import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smap_studio_test_app/bloc/bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;
import 'package:smap_studio_test_app/models/models.dart';

class ItemBloc extends Bloc<ItemSearchEvent, ItemSearchState> {
  @override
  Stream<Transition<ItemSearchEvent, ItemSearchState>> transformEvents(
      Stream<ItemSearchEvent> events, transitionFn) {
    return super.transformEvents(
        events.debounceTime(Duration(milliseconds: 500)), transitionFn);
  }

  @override
  ItemSearchState get initialState => ItemSearchState.initial();

  @override
  void onTransition(Transition<ItemSearchEvent, ItemSearchState> transition) {
    print(transition.currentState.toString());
  }

  @override
  Stream<ItemSearchState> mapEventToState(ItemSearchEvent event) async* {
    yield ItemSearchState.loading();
    try {
      List<JsonItem> items = await _getSearchResults(event.query);
      yield ItemSearchState.success(items, event.query);
    } catch (_) {
      yield ItemSearchState.error();
    }
  }

  Future<List<JsonItem>> _getSearchResults(String query) async {
    var items = List<JsonItem>();
    final response =
        await http.get('http://195.201.56.43:8888/slow_search?q=$query');
    if (response.statusCode == 200) {
      var itemsJson = json.decode(response.body);
      for (var itemJson in itemsJson) {
        items.add(JsonItem.fromJson(itemJson));
      }

      return items;
    } else {
      throw Exception('Failed to load item');
    }
  }
}

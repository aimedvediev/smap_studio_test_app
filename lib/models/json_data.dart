class JsonItem {
  final text;
  final number;

  JsonItem({
    this.text,
    this.number,
  });

  factory JsonItem.fromJson(List<dynamic> json) {
    return JsonItem(
      text: json[0],
      number: json[1],
    );
  }

}


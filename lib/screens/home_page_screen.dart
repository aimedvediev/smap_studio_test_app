import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smap_studio_test_app/bloc/bloc.dart';
import 'package:smap_studio_test_app/models/models.dart';
import 'package:smap_studio_test_app/widgets/widgets.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Search Delegate'),
      ),
      body: Container(
        child: Center(
          child: IconButton(
            onPressed: () async {
              JsonItem selected = await showSearch<JsonItem>(
                context: context,
                delegate: JsonItemSearch(BlocProvider.of<ItemBloc>(context)),
              );
            },
            icon: Icon(Icons.search),
          ),
        ),
      ),
    );
  }
}
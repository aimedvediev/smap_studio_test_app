import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smap_studio_test_app/bloc/bloc.dart';
import 'package:smap_studio_test_app/models/models.dart';

class JsonItemSearch extends SearchDelegate<JsonItem> {
  final Bloc<ItemSearchEvent, ItemSearchState> itemBloc;

  JsonItemSearch(this.itemBloc);

  @override
  List<Widget> buildActions(BuildContext context) {
    return null;
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {}

  @override
  Widget buildSuggestions(BuildContext context) {
    query.isEmpty ? [] : itemBloc.add(ItemSearchEvent(query));
    return BlocBuilder(
      bloc: itemBloc,
      builder: (BuildContext context, ItemSearchState state) {
        List<JsonItem> jsonItems =
            state.items.where((item) => item.text.startsWith(query)).toList();
        if (query.isEmpty) {
          return Container();
        }
        //state.oldQuery for ignore old data if continue typing
        if (state.isLoading || state.oldQuery != query) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state.hasError) {
          return Container(
            child: Text(state.toString()),
          );
        }
        return ListView.builder(
          itemBuilder: (context, index) {
            return Column(
              children: <Widget>[
                ListTile(
                  title: Text(jsonItems[index].text),
                  subtitle: Text(jsonItems[index].number.toString()),
                ),
                Divider(
                  thickness: 1,
                ),
              ],
            );
          },
          itemCount: jsonItems.length,
        );
      },
    );
  }
}
